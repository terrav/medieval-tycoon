package com.terrav.medieval_tycoon.habrahabr

import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable
import com.terrav.medieval_tycoon.AppConstants
import com.terrav.medieval_tycoon.uiSkin

class DrawableStage : Stage() {

    init {
        val stageLayout = Table()
        addActor(stageLayout.apply {
            debugAll()
            setFillParent(true)

            pad(AppConstants.PADDING)
            defaults().expand().fill().space(AppConstants.PADDING)

            row().let {
                val drawable = TextureRegionDrawable(uiSkin.getRegion("sample"))
                add(Image(drawable))
            }

            row().let {
                val drawable: TiledDrawable = TiledDrawable(uiSkin.getRegion("sample"))
                add(Image(drawable))
            }

            row().let {
                val drawable: NinePatchDrawable = NinePatchDrawable(uiSkin.getPatch("sample"))
                add(Image(drawable))
            }
        })
    }
}