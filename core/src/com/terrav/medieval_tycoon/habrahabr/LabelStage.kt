package com.terrav.medieval_tycoon.habrahabr

import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.terrav.medieval_tycoon.AppConstants
import com.terrav.medieval_tycoon.uiSkin

class LabelStage : Stage() {

    init {
        val stageLayout = Table()
        addActor(stageLayout.apply {
            debugAll()
            setFillParent(true)

            pad(AppConstants.PADDING)
            defaults().expand().fill().uniform().space(AppConstants.PADDING)

            row().let {
                add(Label("Lorem ipsum dolor sit amet", uiSkin)).fill(false).top()
                add(Label("Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне.", uiSkin).apply {
                    setWrap(true)
                })
            }

            row().let {
                add(Label("Lorem ipsum dolor sit amet", uiSkin, "large")).fill(false).top()
                add(Label("Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне.", uiSkin, "large").apply {
                    setWrap(true)
                })
            }

            row().let {
                add(Label("Lorem ipsum dolor sit amet", uiSkin, "small")).fill(false).top()
                add(Label("Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне.", uiSkin, "small").apply {
                    setWrap(true)
                })
            }
        })
    }
}