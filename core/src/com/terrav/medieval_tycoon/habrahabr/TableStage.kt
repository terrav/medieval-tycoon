package com.terrav.medieval_tycoon.habrahabr

import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.terrav.medieval_tycoon.AppConstants
import com.terrav.medieval_tycoon.uiSkin

class TableStage : Stage() {

    init {

        val stageLayout = Table()
        addActor(stageLayout.apply {
            debugAll()
            setFillParent(true)

            pad(AppConstants.PADDING)
            defaults().expand().space(AppConstants.PADDING)

            row().let {
                add(Image(uiSkin.getDrawable("sample")))
                add(Image(uiSkin.getDrawable("sample"))).top().right()
                add(Image(uiSkin.getDrawable("sample"))).fill()
            }

            row().let {
                add(Image(uiSkin.getTiledDrawable("sample"))).fillY().left().colspan(2)
                add(Image(uiSkin.getTiledDrawable("sample"))).width(64f).height(64f).right().bottom()
            }

            row().let {
                add(Image(uiSkin.getDrawable("sample")))
                add(Image(uiSkin.getTiledDrawable("sample"))).fill().pad(AppConstants.PADDING)
                add(Image(uiSkin.getDrawable("sample"))).width(64f).height(64f)
            }
        })
    }
}