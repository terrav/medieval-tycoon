package com.terrav.medieval_tycoon

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Scaling
import com.badlogic.gdx.utils.Timer
import com.terrav.medieval_tycoon.main.MainScreen

class LoadingScreen : ScreenAdapter() {

    private val loadingStage = LoadingStage()

    override fun hide() {
        Gdx.input.inputProcessor = null
    }

    override fun show() {
        Gdx.input.inputProcessor = loadingStage
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 0f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        loadingStage.act(delta)
        loadingStage.draw()
    }

    override fun resize(width: Int, height: Int) {
        viewport.update(width, height)
    }
}

class LoadingStage : Stage(viewport) {

    init {
        val backgroundImage = Image(Texture("backgrounds/loading-logo.png"))
        addActor(backgroundImage.apply {
            setFillParent(true)
            setScaling(Scaling.fill)

            Timer.schedule(object: Timer.Task() {
                override fun run() {
                    currentScreen = MainScreen()
                }
            }, 1f)
        })
    }
}