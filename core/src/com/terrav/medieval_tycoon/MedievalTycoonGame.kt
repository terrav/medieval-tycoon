package com.terrav.medieval_tycoon

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.I18NBundle
import com.badlogic.gdx.utils.viewport.FitViewport
import com.terrav.medieval_tycoon.habrahabr.ShowcaseScreen

object viewport : FitViewport(AppConstants.APP_WIDTH, AppConstants.APP_HEIGHT)

var currentScreen: Screen
    get() = (Gdx.app.applicationListener as Game).screen
    set(value) {
        (Gdx.app.applicationListener as Game).screen = value
    }

val assets: Assets
    get() = (Gdx.app.applicationListener as MedievalTycoonGame).assets


val uiSkin: Skin
    get() = assets.uiSkin

val i18n: I18NBundle
    get() = assets.i18n

class MedievalTycoonGame : Game() {

    lateinit var assets: Assets

    override fun create() {
        assets = Assets()

        // currentScreen = ShowcaseScreen()
        currentScreen = LoadingScreen()
    }
}
