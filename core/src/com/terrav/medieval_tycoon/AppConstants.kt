package com.terrav.medieval_tycoon

object AppConstants {

    val APP_WIDTH = 1080f
    val APP_HEIGHT = 1920f

    val PADDING = 20f
}