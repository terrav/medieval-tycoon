package com.terrav.medieval_tycoon.main

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Container
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.terrav.medieval_tycoon.AppConstants
import com.terrav.medieval_tycoon.viewport

class MainStage : Stage(viewport) {

    init {

        val stageLayout = Table()
        addActor(stageLayout.apply {
            setFillParent(true)

            defaults().fill()

            row().let {
                val headerContainer = Container<WidgetGroup>()
                add(headerContainer.apply {
                    background = TextureRegionDrawable(TextureRegion(Texture("images/status-bar-background.png")))
                }).height(100f).expandX()
            }

            val centralPanel = Container<WidgetGroup>()
            row().let {
                add(centralPanel.apply {
                    background = TextureRegionDrawable(TextureRegion(Texture("backgrounds/main-screen-background.png")))
                    fill()
                    pad(AppConstants.PADDING * 2)
                }).expand()
            }

            row().let {
                val footerContainer = Container<WidgetGroup>()
                add(footerContainer.apply {
                    background = TextureRegionDrawable(TextureRegion(Texture("images/status-bar-background.png")))
                    fill()

                    actor = CommandPanel(centralPanel)
                }).height(160f).expandX()
            }
        })
    }
}