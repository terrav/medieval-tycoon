package com.terrav.medieval_tycoon.main.explore

import com.badlogic.gdx.scenes.scene2d.ui.Button
import com.terrav.medieval_tycoon.AppConstants
import com.terrav.medieval_tycoon.uiSkin

open class WoodenPane : Button(uiSkin.getDrawable("wooden-pane-background")) {

    init {
        pad(AppConstants.PADDING * 2, AppConstants.PADDING * 4, AppConstants.PADDING * 2, AppConstants.PADDING * 4)
        defaults().space(AppConstants.PADDING)
    }
}