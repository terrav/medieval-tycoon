package com.terrav.medieval_tycoon.main.explore

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.terrav.medieval_tycoon.AppConstants
import com.terrav.medieval_tycoon.i18n
import com.terrav.medieval_tycoon.uiSkin

class TerrainPane : WoodenPane() {

    init {

        add(Image(uiSkin.getDrawable("terrain-meadow"))).width(160f).height(160f).top()

        add(VerticalGroup().apply {
            space(AppConstants.PADDING)

            addActor(Label(i18n["terrain.meadow"], uiSkin, "pane-caption"))

            addActor(HorizontalGroup().apply {
                space(AppConstants.PADDING)

                addActor(Image(uiSkin.getDrawable("herbs-01")))
                addActor(Image(uiSkin.getDrawable("herbs-unidentified")))
                addActor(Image(uiSkin.getDrawable("herbs-unidentified")))
                addActor(Image(uiSkin.getDrawable("herbs-unidentified")))
                addActor(Image(uiSkin.getDrawable("herbs-unidentified")))
            })
        }).expandX().fill()
    }
}