package com.terrav.medieval_tycoon.main.explore

import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.terrav.medieval_tycoon.AppConstants
import com.terrav.medieval_tycoon.uiSkin

class ExplorePanel : Table() {

    init {
        background = uiSkin.getDrawable("panel-background")
        pad(AppConstants.PADDING)

        row().let {
            add(TerrainPane())
        }

        row().let {
            add(SearchPane())
        }

        row().let {
            add(MovePane())
        }

        row().let {
            add(TownPortalPane())
        }

        row().let {
            add().expand()
        }
    }
}