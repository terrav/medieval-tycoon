package com.terrav.medieval_tycoon.main.explore

import com.badlogic.gdx.scenes.scene2d.ui.*
import com.terrav.medieval_tycoon.AppConstants
import com.terrav.medieval_tycoon.i18n
import com.terrav.medieval_tycoon.uiSkin

class TownPortalPane : Table() {

    init {
        background = uiSkin.getDrawable("wooden-pane-background")
        pad(AppConstants.PADDING)
        defaults().space(AppConstants.PADDING)

        add(Image(uiSkin.getDrawable("explore-town-portal"))).top()

        add(VerticalGroup().apply {
            space(AppConstants.PADDING)
            expand()
            fill()

            addActor(Label(i18n["explore.town-portal"], uiSkin, "large"))

            addActor(HorizontalGroup().apply {
                addActor(Image(uiSkin.getDrawable("resource-scroll-64")))
                addActor(Label("PH", uiSkin))
            })

            addActor(ProgressBar(0f, 1f, .001f, false, ProgressBar.ProgressBarStyle()).apply {
                style.background = uiSkin.getDrawable("progress-bar-empty").apply {
                    minWidth = 0f
                }

                style.knobBefore = uiSkin.getDrawable("progress-bar-filled").apply {
                    minWidth = 0f
                }
            })
        }).expandX().fill()
    }
}