package com.terrav.medieval_tycoon.main

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.GL20
import com.terrav.medieval_tycoon.viewport

class MainScreen : ScreenAdapter() {

    val mainStage = MainStage()

    override fun hide() {
        Gdx.input.inputProcessor = null
    }

    override fun show() {
        Gdx.input.inputProcessor = mainStage
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 0f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        mainStage.act(delta)
        mainStage.draw()
    }

    override fun resize(width: Int, height: Int) {
        viewport.update(width, height)
    }
}