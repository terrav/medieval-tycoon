package com.terrav.medieval_tycoon.main

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.terrav.medieval_tycoon.AppConstants
import com.terrav.medieval_tycoon.main.explore.ExplorePanel
import com.terrav.medieval_tycoon.uiSkin

class CommandPanel(val centralPanel: Container<WidgetGroup>) : Table() {

    private val commandButtonsGroup = ButtonGroup<Button>().apply {
        setMinCheckCount(0)
    }

    init {

        defaults().space(AppConstants.PADDING)

        apply {
            add(Button(uiSkin.getDrawable("command-move")).apply {
                addListener(object : ChangeListener() {
                    override fun changed(event: ChangeEvent?, actor: Actor?) {
                        when (isChecked) {
                            false -> centralPanel.actor = null
                            true -> centralPanel.actor = ExplorePanel()
                        }
                    }
                })
            })

            add(Button(uiSkin.getDrawable("command-trade")).apply {

            })

            add(Button(uiSkin.getDrawable("command-sack")).apply {

            })

            add(Button(uiSkin.getDrawable("command-storage")).apply {

            })
        }
    }
}