package com.terrav.medieval_tycoon.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.terrav.medieval_tycoon.MedievalTycoonGame

object DesktopLauncher {
    @JvmStatic fun main(arg: Array<String>) {
        val config = LwjglApplicationConfiguration()
        config.width = 576
        config.height = 1024
        config.resizable = false
        config.vSyncEnabled = false
        LwjglApplication(MedievalTycoonGame(), config)
    }
}
