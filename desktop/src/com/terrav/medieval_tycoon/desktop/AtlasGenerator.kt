package com.terrav.medieval_tycoon.desktop

import com.badlogic.gdx.tools.texturepacker.TexturePacker

object AtlasGenerator {

    @JvmStatic fun main(args: Array<String>) {
        val settings = TexturePacker.Settings()
        settings.maxWidth = 2048
        settings.maxHeight = 2048
        TexturePacker.process(settings, "images", "atlas", "game")
    }
}
